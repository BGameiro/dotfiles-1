[Appearance]
BoldIntense=true
ColorScheme=Gruvbox_Dark_Real_Colors
Font=Source Code Pro for Powerline,9,-1,5,50,0,0,0,0,0,Regular
UseFontLineChararacters=false

[Cursor Options]
CursorShape=0
CustomCursorColor=255,255,255

[General]
Command=/bin/tmux
Directory=
Icon=terminix
Name=MyProfile
Parent=FALLBACK/
StartInCurrentSessionDir=false
TerminalColumns=100
TerminalRows=60

[Scrolling]
HistoryMode=2
HistorySize=2400
ScrollBarPosition=2

[Terminal Features]
FlowControlEnabled=false
